import { Component, OnInit } from '@angular/core';
import { Request } from '../models/request';
import { RequestService } from '../services/request.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {
  requests: Request[] = [];

  constructor(private requestService: RequestService) { }

  ngOnInit(): void {
    this.requestService.requests$.subscribe(requests => {
      this.requests = requests;
    });
    this.requestService.getRequests();
  }

  showObject(obj) {
    return JSON.stringify(obj);
  }

}
