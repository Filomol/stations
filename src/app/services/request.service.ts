import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Request } from '../models/request';

@Injectable({
    providedIn: 'root'
})
export class RequestService {
    private requests: Request[] = [];
    private requestsSubject = new BehaviorSubject(this.requests);

    public requests$ = this.requestsSubject.asObservable();

    constructor() { }

    public getRequests() {
        return this.requests;
    }

    public createRequest(request: Request) {
        request.id = this.requests.length;
        this.requests.push(request);
        this.requestsSubject.next(this.requests);
    }
}