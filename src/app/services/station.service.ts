import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { Station } from '../models/station';

@Injectable({
  providedIn: 'root'
})
export class StationService {
  private stationsPath = '../assets/charging_station_msk.json';
  private stationsSubject = new BehaviorSubject(null);

  public stations$ = this.stationsSubject.asObservable();

  constructor(private http: HttpClient) { }

  loadStations() {
    this.http.get<any>(this.stationsPath).subscribe((data) => {
      let stations: Station[];
      stations = data.features.map(feature => {
        return {
          id: (parseInt(feature.id.replace(/\D+/g,""))), operator: feature.properties.operator,
          description: feature.properties.description, website: feature.properties.website,
          socket_type2: feature.properties["socket:type2"] ? feature.properties["socket:type2"] : 0,
          socket_yazaki: feature.properties["socket:yazaki"] ? feature.properties["socket:yazaki"] : 0,
          socket_schuko: feature.properties["socket:schuko"] ? feature.properties["socket:schuko"] : 0,
          lat: feature.geometry.coordinates[1], lng: feature.geometry.coordinates[0]
        };
      });
      this.stationsSubject.next(stations);
    }, error => console.log(error));
  }
}