import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRequstComponent } from './create-requst.component';

describe('CreateRequstComponent', () => {
  let component: CreateRequstComponent;
  let fixture: ComponentFixture<CreateRequstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateRequstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRequstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
