import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Station } from '../models/station';
import { StationService } from '../services/station.service';
import { Location } from '@angular/common';
import { Request } from '../models/request';
import { RequestService } from '../services/request.service';

@Component({
  selector: 'app-create-requst',
  templateUrl: './create-requst.component.html',
  styleUrls: ['./create-requst.component.css']
})
export class CreateRequstComponent implements OnInit {
  private _stationOriginal: Station;

  private stations: Station[];
  
  stationForEdit: Station;
  typeOfEditing: string;
  editInfoType = "Изменить информацию";
  editSocketType = "Изменить сокет";
  deleting = "Удалить объект";
  reasonOfDeleting: string;

  get stationOriginal() {
    return this._stationOriginal;
  }

  set stationOriginal(station: Station) {
    this._stationOriginal = station;
    this.stationForEdit = new Station();
    Object.assign(this.stationForEdit, this._stationOriginal);
  }

  constructor(private stationService: StationService,
              private requstService: RequestService,
              private route: ActivatedRoute,
              private location: Location) { }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.stationService.stations$.subscribe(stations => {
      this.stations = stations;
      if (this.stations) {
        this.stationOriginal = this.stations.find(station => station.id === id);
      }
    });
    this.stationService.loadStations();
    this.typeOfEditing = this.editInfoType;
  }

  canSend() {
    return this.stationForEdit && ((this.stationForEdit.operator && this.stationInfoChanged()) ||
      this.stationSocketChanged() || this.reasonOfDeleting);
  }

  private stationInfoChanged() {
    return this.stationForEdit.description !== this.stationOriginal.description ||
      this.stationForEdit.website !== this.stationOriginal.website ||
      this.stationForEdit.operator !== this.stationOriginal.operator;
  }

  private stationSocketChanged() {
    return this.stationForEdit.socket_type2 != this.stationOriginal.socket_type2 ||
      this.stationForEdit.socket_yazaki != this.stationOriginal.socket_yazaki ||
      this.stationForEdit.socket_schuko != this.stationOriginal.socket_schuko;
  }

  sendRequest() {
    if (this.canSend()) {
      let newRequest = new Request();
      newRequest.id = -1;
      newRequest.object_id = this.stationOriginal.id;
      newRequest.type = this.typeOfEditing;
      if (this.typeOfEditing === this.editInfoType || this.typeOfEditing === this.editSocketType) {
        newRequest.data = JSON.parse(JSON.stringify(this.stationForEdit));
      } else if (this.typeOfEditing === this.deleting) {
        newRequest.data = JSON.parse(JSON.stringify({
          reasonOfDeleting: this.reasonOfDeleting
        }));
      }
      this.requstService.createRequest(newRequest);
      this.goBack();
    }
  }

  resetChanges() {
    Object.assign(this.stationForEdit, this._stationOriginal);
  }

  goBack() {
    this.location.back();
  }
}
