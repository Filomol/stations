import { Component, OnInit } from '@angular/core';
import { Station } from '../models/station';
import { StationService } from '../services/station.service';
import * as L from 'leaflet';

@Component({
  selector: 'app-all-stations',
  templateUrl: './all-stations.component.html',
  styleUrls: ['./all-stations.component.css']
})
export class AllStationsComponent implements OnInit {
  private map: L.Map;
  private markers: L.Marker[] = [];

  stations: Station[] = [];

  constructor(private stationService: StationService) { }

  ngOnInit(): void {
    this.initMap();
    this.stationService.stations$.subscribe(stations => {
      this.stations = stations;
      this.showStationsInMap();
    });
    this.stationService.loadStations();
  }

  private initMap() {
    this.map = L.map('mapid').setView([55.7522200, 37.6155600], 13);
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
    }).addTo(this.map);
  }

  private showStationsInMap() {
    if (this.stations) {
      this.stations.forEach(s => {
        this.markers.push(L.marker([s.lat, s.lng]));
      });
      this.markers.forEach(m => {
        m.addTo(this.map);
      });
    }
  }
}
