import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllStationsComponent } from './all-stations/all-stations.component';
import { CreateRequstComponent } from './create-requst/create-requst.component';
import { RequestsComponent } from './requests/requests.component';


const routes: Routes = [
  { path: 'all-stations', component: AllStationsComponent },
  { path: 'requests', component: RequestsComponent },
  { path: 'all-stations/:id', component: CreateRequstComponent },
  { path: '',   redirectTo: '/all-stations', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
