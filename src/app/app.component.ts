import { Component } from '@angular/core';
import { icon, Marker } from 'leaflet';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'transport-charging-stations';

  constructor() {
    this.fixLeafletMarker();
  }

  private fixLeafletMarker() {
    const iconUrl = 'assets/marker-icon.png';
    const shadowUrl = 'assets/marker-shadow.png';
    const iconDefault = icon({
      iconUrl,
      shadowUrl
    });
    Marker.prototype.options.icon = iconDefault;
  }
}
