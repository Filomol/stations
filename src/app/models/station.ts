export class Station {
  id: number;
  operator: string;
  description: string;
  website: string;
  socket_type2: number;
  socket_yazaki: number;
  socket_schuko: number;
  lat: number;
  lng: number;
}