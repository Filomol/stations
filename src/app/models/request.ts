export class Request {
    id: number;
    object_id: number;
    type: string;
    data: string;
}